<?php
/**
 * @file
 * Utility functions and page callbacks for content_staging module.
 * 
 * @todo write README
 **** release at this point
 * @todo make checkout before update optional
 * @todo allow checkout script to be configurable (e.g. to use Git rather than subversion)
 * @todo only process files with newer build than on website (to save time)
 * @todo allow file types other than .txt ?
 * @todo Drupal 7 version
 * @todo update on cron
 * @todo Allow to work with Git using gitattribute Keyword Expansion and smudge: http://progit.org/book/ch7-2.html
 */

/**
 * Run Writeup on a file
 * 
 * @param $file
 *   path of file to be processed
 *
 * @param $options
 *   options to be passed to Writeup
 *
 * @param &$output
 *   array containing output from Writeup process (passed by reference)
 *
 * @param $see_errors
 *   Include error message sent to stdout (to supress normal output and just see errors, use -x NULL)
 *
 * @return
 *   passes the Writeup return code
 */
function _cs_writeup_exec($file, $options, &$output, $see_errors=FALSE) {
  $stderr = see_errors ? ' 2>&1' : '';
  $writeup = rtrim(variable_get('writeup_loc', ''), '/\\') . '/writeup';
  $retvar = 0;
  exec( $writeup . ' -ybqrf drupal ' . $options . ' ' . $file . $stderr, $output, $retvar ); //run quietly, redirecting to stdout
  return ($retvar);
}

/**
 * Build array of keywords extracted from a file
 * 
 * @param $file
 *   path of file to be processed
 *
 * @param &$keywords
 *   array containing keywords and values
 *
 * @return
 *   passes the Writeup return code
 */
function writeup_keywords($file, &$keywords=array()) {
  $output=array();
  $keywords['RETCODE'] = _cs_writeup_exec($file, '-X "*"', $output);
  foreach ($output as $line) {
    $equals = strpos($line, '=');
    $keywords[substr($line, 0, $equals)] = substr($line, $equals+2, -1);
  }
  return ($keywords['RETCODE']);
}

/**
 * Get build number of a document. If no SVN, use date
 * 
 * @param $keywords
 *   array of keywords returned by writeup_keywords
 *
 * @return
 *   build number, or date in numeric form YYYYMMDD
 */
function _cs_getDocBuild($keywords) {
  if (isset($keywords['SVNBUILD'])) return $keywords['SVNBUILD'];
  else return substr($keywords['DATE'],0,4) . substr($keywords['DATE'],5,2) . substr($keywords['DATE'],8,2);
}

/**
 * Get filename from path, including path offset from the root, but not the root name
 * e.g. pub/subdir/filename.txt -> subdir/filename
 * 
 * @param $path
 *   full path
 *
 * @return
 *   filename including partial path, but no file extension
 */
function _cs_getName($path) {
  $rootlen = strlen(variable_get('content_staging_dir', '')) + 1; // plus 1 for the '/' separator
  return substr($path, $rootlen, -4); // -4 for '.txt' This will have to be changed if other extensions allowed
}

/**
 * Get build number of Drupal node
 * The node_revisions table is used to story the build number
 * 
 * @param $nid
 *   node id
 *
 * @return
 *   current build number of node
 */
function _cs_getNodeBuild($nid) {    
  $sql="SELECT log FROM {node} JOIN {node_revisions} ON {node}.vid = {node_revisions}.vid WHERE {node}.nid=%d;"; // find 
  return db_result(db_query($sql,$nid));
}

/**
 * Get the Node ID corresponding with a path, and therefore a URL alias
 * 
 * @param $path
 *   path
 *
 * @return
 *   node id or -1 if not found 
 */
function _cs_getNid($path) {
  $sql="SELECT src FROM {url_alias} WHERE dst='%s';"; // get id of node
  $src = db_result(db_query($sql, _cs_getName($path)));
  if ($src == FALSE) return -1;
  else return (int)substr($src, 5); //todo: why substr?
}

/**
 * Generate a list of all files in a directory and all subdirectories that have a given file extension.
 * 
 * @param $rootDir
 *   root directory for the list, with no trailing '/'
 *
 * @param $allowext
 *   array of permissible file extensions
 *
 * @param $allData
 *   resulting array, passed by reference
 *
 * @return
 *   resulting array 
 */
function _cs_scanDirectories($rootDir, $allowext, &$allData = array()) {
  $dirContent = scandir($rootDir);
  foreach($dirContent as $key => $content) {
    if ($content == '.' || $content == '..') continue;
    $path = $rootDir . DIRECTORY_SEPARATOR . $content;
    $ext = substr($content, strrpos($content, '.') + 1);
    if(in_array($ext, $allowext) && is_file($path) && is_readable($path)) {
      $allData[] = $path;
    }
    else if(is_dir($path) && is_readable($path)) {
      // recursive callback to open new directory
      _cs_scanDirectories($path, $allowext, $allData);
    }
    // else unreadable garbage
  }
  return $allData;
}

/**
 * Generate a table of all staged files, showing their status.
 * 
 * @return
 *   resulting page 
 */
function content_staging_file_status_page() {
  $dir = variable_get('content_staging_dir', '');
  if ($dir == '') return t('Error: Content staging directory is not yet set up. Please go to the !url and configure it.', array('!url' => l(t('admin page'), 'admin/settings/content_staging')));
  $files = _cs_scanDirectories($dir, array("txt"));
  $page = '<h2>' . t('List of files in content staging') . '</h2>'
        . '<p>' . t('To update files, go to the !url.', array('!url' => l(t('update page'), 'cs_update'))) . '</p>';
  $page .='<table><tr><th>' . t('Filename') . '</th><th>' . t('Type') . '</th><th>'
          . t('File ver') . '</th><th>' . t('Node ver') . '</th><th>nid</th><th>'
          . t('Timestamp') . '</th><th>' . t('Title') . '</th><tr>';
  foreach ($files as $file) {
    if (substr($file,-4) != '.txt') continue;
    $keywords = array();
    writeup_keywords($file, $keywords);
    if ($keywords['CS_UPDATE'] != 'TRUE') continue;
    $nid = _cs_getNid($file);
    $page .= '<tr><td>' . l(_cs_getName($file), 'cs_listvars/' . _cs_getName($file));
    $page .= '</td><td>' . $keywords['TYPE'];
    $page .= '</td><td>' . _cs_getDocBuild($keywords);
    $page .= '</td><td>' . _cs_getNodeBuild($nid);
    $page .= '</td><td>' . l($nid, 'cs_listnode/' . $nid);
    $page .= '</td><td>' . $keywords['SVNDATE'] . ' ' . $keywords['SVNTIME'];
    $page .= '</td><td>';
    if ($keywords['RETCODE'] != 0) {
      $page .= '<strong>'
        . l($keywords['RETCODE'] == 1 ? t('Warning') : t('Error'), 'cs_listerr/' . _cs_getName($file))
        . '</strong> ';
    }
    $page .= $keywords['TITLE'];
    $page .= '</td></tr>';
  }
  $page .= '</table>';
  return t($page);
}

/**
 * Update node from file
 * 
 * @param $filename
 *   filename (corresponds with URL alias of target node)
 *
 * @return
 *   result to be displayed in update status log page 
 */
function content_staging_update_node($filename) {
  $keywords = array();
  writeup_keywords($filename, $keywords);
  $nid = _cs_getNid($filename);
  if (($nid == -1) || ($keywords['RETCODE'] != 0) || ($keywords['TYPE'] != 'writeup')) return '';
  $log = '<tr><td>' . $filename . '</td><td>' . $nid . '</td><td>' . $keywords['TITLE'] . '</td><td>';

  // make changes to node using node_load and node_save --slow but accurate
  $node = node_load($nid); 
  if ($node == FALSE ) return $log . t('Invalid node') . '</td></tr>';

  // update node object
  $node->title = $keywords['TITLE'];
  $node->format = 2; //todo: this should be changeable
  $node->teaser = '';
  if (isset($keywords['CS_PUBLISH'])) $node->status  = $keywords['CS_PUBLISH'];
  if (isset($keywords['CS_PROMOTE'])) $node->promote = $keywords['CS_PROMOTE'];
  if (isset($keywords['CS_STICKY']))  $node->sticky  = $keywords['CS_STICKY'];

  // process body and put it into database
  $output=array();
  _cs_writeup_exec($filename, '', $output);
  $node->body = implode("\n", $output);
  $node->log = _cs_getDocBuild($keywords); // put version/build number in log
  $node = node_submit($node);
  node_save($node);
  if (isset($keywords['SVNDATE']) && isset($keywords['SVNTIME'])) {  // have to manually set timestamps
    $svntimestamp = strtotime($keywords['SVNDATE'] . ' ' . $keywords['SVNTIME']);
    $sql = "UPDATE {node} SET changed=%d, created=%d WHERE nid=%d;";
    db_query($sql, $svntimestamp, $svntimestamp,$nid);
  }
  return $log . '<em>' . t('updated') . '</em></td></tr>';
}

/**
 * Update node from file
 *   Alternative that makes changes to node using direct database writes -- fast but misses things
 * 
 * @param $filename
 *   filename (corresponds with URL alias of target node)
 *
 * @return
 *   result to be displayed in update status log page 
 */
function content_staging_update_db($filename) { // 
  $keywords = array();
  writeup_keywords($filename, $keywords);
  $nid = _cs_getNid($filename);
  if (($nid == -1) || ($keywords['RETCODE'] != 0) || ($keywords['TYPE'] != 'writeup')) return '';
  $log = '<tr><td>' . $filename . '</td><td>' . $nid . '</td><td>' . $keywords['TITLE'] . '</td><td>';

  $sql="SELECT vid FROM {node} WHERE nid=%d;"; // get number of revision
  $vid = db_result(db_query($sql,$nid));
  
  // process title and other node attributes and put it into database
  $sql = "UPDATE {node} SET title='%s'";
  $sqlFields = array($keywords['TITLE']);
  if (isset($keywords['CS_PUBLISH'])) {
    $sql .= ", status=%d";
    $sqlFields[] = $keywords['CS_PUBLISH'];
  }
  if (isset($keywords['CS_PROMOTE'])) {
    $sql .= ", promote=%d";
    $sqlFields[] = $keywords['CS_PROMOTE'];
  }
  if (isset($keywords['CS_STICKY'])) {
    $sql .= ", sticky=%d";
    $sqlFields[] = $keywords['CS_STICKY'];
  }
  $sql .= " WHERE nid=%d;";
  $sqlFields[] = $nid;
  db_query($sql,$sqlFields);

  // process body and put it into database
  $output=array();
  _cs_writeup_exec($filename, '', $output);
  $newcontent = implode("\n", $output);
  $sql="UPDATE {node_revisions} SET title='%s', body='%s', teaser='', format=2, log='%s' WHERE vid=%d;"; // format 2=full HTML todo: this should be changeable
  //$log .= '<br />SQL=' . $sql;
  db_query($sql, $keywords['TITLE'], $newcontent, _cs_getDocBuild($keywords), $vid);
  return $log . '<em>' . t('updated') . '</em></td></tr>';
}

/**
 * Update all nodes that have associated files
 * 
 * @return
 *   resulting update status log page 
 */
function content_staging_update_page() {
  $page = '<h2>Updating content from staging</h2>';
  $lastline = exec( 'svn up --config-dir /var/www/html pub'); //may need to set this up if password needed todo: make this line configurable
  $page .= '<pre>' . $lastline  . '</pre><hr />';

  $dir   = variable_get('content_staging_dir', 'staging');  //find all the files
  $files = _cs_scanDirectories($dir, array("txt"));
  $page .= '<table><tr><th>File</th><th>nid</th><th>Title</th><th>Result</th></tr>';
  foreach ($files as $file) {
    if ( (substr($file,-4) != '.txt')) continue;
    $page .= content_staging_update_node($file);
  }
  $page .= '</table>';
  return t($page);
}

/**
 * List variables in a file for diagnostic purposes
 * 
 * @param $file
 *   file path
 *
 * @return
 *   page containing file variables printed out with print_r
 */
function content_staging_list_variables($file = '') {
  if ($file == '') return '<p>' . t('No file specified') . '</p>';
  $path = variable_get('content_staging_dir', 'staging');
  for ($i = 1; !is_null(arg($i)); $i++) $path .= '/' . arg($i); // build path from arguments
  $path .= '.txt';
  if (!file_exists($path)) return '<p>' . t('Path does not exist: @path', array('@path' => $path)) . '</p>';
  $page = '<h2>Listing variables in file ' . $path . '</h2>';
  $keywords=array();
  writeup_keywords($path, $keywords) . "<br />";
  return $page . '<pre>' . print_r($keywords, TRUE) . '</pre>';
}

/**
 * List error messages from a file
 * 
 * @param $file
 *   file path
 *
 * @return
 *   page containing error message text
 *   if there are no errors or warnings rhwn 
 */
function content_staging_file_errors($file = '') {
  if ($file == '') return '<p>' . t('No file specified') . '</p>';
  $path = variable_get('content_staging_dir', 'staging');
  for ($i = 1; !is_null(arg($i)); $i++) $path .= '/' . arg($i); // build path from arguments
  $path .= '.txt';
  if (!file_exists($path)) return '<p>' . t('Path does not exist: @path', array('@path' => $path)) . '</p>';
  $output=array();
  _cs_writeup_exec($path, '-x NULL', $output, TRUE);
  //if ($retcode == 0) return '<h2>No errors in file: ' . $path . '</h2>';
  //else 
  return '<h2>Errors in file: ' . $path . '</h2><pre>' . implode("\n", $output) . '</pre>';
}

/**
 * List contents of a node for diagnostic purposes
 * 
 * @param $nid
 *   node id
 *
 * @return
 *   page containing node printed out with print_r
 */
function content_staging_list_node($nid = '-1') {
  if ($nid == -1) return t('No node specified');
  $node = node_load($nid);
  if ($node == FALSE ) return t('Invalid node specified');
  $page = '<h2>Listing contents of node ' . $nid . '</h2>';
  $page .= 'title: ' . $node->title . '<br />';
  return $page . '<pre>' . print_r($node, TRUE) . '</pre>';
}


